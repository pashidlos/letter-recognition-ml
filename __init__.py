import os

import numpy as np
import tensorflow as tf

from flask import Flask, request, render_template, jsonify
from flask_uploads import IMAGES, UploadSet, configure_uploads, patch_request_class
from keras.engine.saving import load_model
from keras.preprocessing import image

app = Flask(__name__)
app.config['UPLOADED_PHOTOS_DEST'] = os.getcwd() + '/static/'

photos = UploadSet('photos', IMAGES)
configure_uploads(app, photos)
patch_request_class(app)  # set maximum file size, default is 16MB

MODEL_PATH = 'model/my_model.h5'

img_rows = 32
img_cols = 32
img_color_channels = 3
num_classes = 33

alphabet = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
            'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
            'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']

print("start loading model")
model = load_model(MODEL_PATH)
print("finished loading model")
graph = tf.get_default_graph()


def image_to_tensor(file):
    img = image.load_img(app.config['UPLOADED_PHOTOS_DEST'] + file,
                         target_size=(img_rows, img_cols))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x /= 255.
    return x


def make_prediction(file):
    with graph.as_default():
        result_index = model.predict_classes(image_to_tensor(file))
    return alphabet[result_index[0]]


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST' and 'photo' in request.files:
        filename = photos.save(request.files['photo'])
        result = make_prediction(filename)

        return render_template('upload.html', result=result)
    return render_template('upload.html')


@app.route('/predict', methods=['POST'])
def predict():
    filename = photos.save(request.files['photo'])
    result = make_prediction(filename)
    return jsonify(
        letter=result
    )


@app.route('/probabilities', methods=['POST'])
def probabilities():
    image_file = request.files['photo']
    letter_index = request.values['index']
    saved_image = photos.save(image_file, folder=letter_index)

    with graph.as_default():
        result_index = model.predict_proba(image_to_tensor(saved_image))
    result = result_index[0]
    return jsonify(
        probabilities=result.tolist()
    )


if __name__ == '__main__':
    app.run(debug=False)